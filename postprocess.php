<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$sql = "SELECT url FROM profiles WHERE photo='Y'";
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$sql = "SELECT idx,content FROM inbox WHERE post_process='N' AND content LIKE '%".
			mysqli_real_escape_string($conn,$row['url'])."%'";
	$xres = mysqli_query($conn,$sql);
	while ($xrow = mysqli_fetch_array($xres))
	{
		$sql = "UPDATE inbox SET post_process='Y' WHERE idx='".
			mysqli_real_escape_string($conn,$xrow['idx'])."'";
		mysqli_query($conn,$sql);
		$content = json_decode($xrow['content'],true);
		$attachments = $content['object']['attachment'];
		if (is_array($attachments) && (count($attachments)>0))
		{
			foreach ($attachments as $k=>$v)
			{
				if ($v['mediaType']=='image/jpeg')
				{
					$hash = hash('sha256',$v['url'],false);
					if (!file_exists('photos/'.$hash.'.jpg'))
					{
						$dt = date('Y-m-d');
						if (!is_dir('photos/'.$dt))
						{
							mkdir('photos/'.$dt);
							mkdir('photos/th/'.$dt);
						}
						system(WGET.' -O photos/'.$dt.'/'.$hash.'.jpg '.escapeshellarg($v['url']));
						list($w,$h) = getImageSize('photos/'.$dt.'/'.$hash.'.jpg');
						if ($w>0)
						{
							system(CONVERT.' -strip -size 300x300 -scale 300x300 -density 120 photos/'.$dt.'/'.$hash.'.jpg photos/th/'.$dt.'/'.$hash.'.jpg');
							$sql = "INSERT INTO gallery (idx,inbox_idx,dt,image,is_delete,sequence) VALUES (NULL,'".
								mysqli_real_escape_string($conn,$xrow['idx'])."','".
								mysqli_real_escape_string($conn,$dt)."','".
								mysqli_real_escape_string($conn,$hash)."','N','".time()."')";
							mysqli_query($conn,$sql) or die($sql);
						} else {
							unlink('photos/'.$hash.'.jpg');
						}
					}
				}
			}
		}

	}
	mysqli_free_result($xres);
}
mysqli_free_result($res);
mysqli_close($conn);


