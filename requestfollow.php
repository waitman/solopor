<?php

define('SIGNIT',false);

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

require('LDSignatures.php');

include('config.php');
include(DBFILE);

$keys = json_decode(file_get_contents(KEYPATH),true);

$p = json_decode(file_get_contents(ACTORPATH),true);
$actor = $p['url'];

$channel=array();
$channel['prvkey']=$keys['prvkey'];
$channel['url']=$actor;

$addr = $argv[1];
$inbox = '';

if ($addr=='') exit("\n".'no address'."\n");

$sql = "SELECT * FROM profiles WHERE url='".
        mysqli_real_escape_string($conn,$addr)."'";
$res = mysqli_query($conn,$sql);
if (mysqli_num_rows($res)<1)
{
        mysqli_free_result($res);
        echo "\nError - Profile does not exist\n\n";
        exit();
} else {
	$row = mysqli_fetch_array($res);
	$jd = json_decode($row['content'],true);
	$inbox = $jd['inbox'];
	$profile_idx = $row['idx'];
}
mysqli_free_result($res);

if ($inbox=='') exit("\nMissing Inbox\n\n");

$msg_id = 'https://solopor.com/msgid/'.bin2hex(random_bytes(16));


if (SIGNIT) {
$msg = array_merge(['@context' => [
				ACTIVITYSTREAMS_JSONLD_REV,
				'https://w3id.org/security/v1'
			]],
                        [
                                'id'     => $msg_id,
                                'type'   => ACTIVITY_FOLLOW,
                                'actor'  => $actor,
				'object' => $addr,
                                'to'     => [ $addr ]
			]);

		$msg['signature'] = LDSignatures::sign($msg,$channel);

} else {
                     $msg =   [
				'@context' => ACTIVITYSTREAMS_JSONLD_REV,
                                'id'     => $msg_id,
                                'type'   => ACTIVITY_FOLLOW,
                                'actor'  => $actor,
                                'object' => $addr
                        ];
}


                $jmsg = json_encode($msg, JSON_UNESCAPED_SLASHES);

$sql = "INSERT INTO queue (idx,profile_idx,msg_id,posturl,created,delivered,scheduled,response,msg) VALUES (NULL,'".
	mysqli_real_escape_string($conn,$profile_idx)."','".
	mysqli_real_escape_string($conn,$msg_id)."','".
	mysqli_real_escape_string($conn,$inbox)."','".time()."','0','0','','".
	mysqli_real_escape_string($conn,$jmsg)."')";
mysqli_query($conn,$sql) or die($sql);

mysqli_close($conn);

echo "\n".$msg_id." Queued.\n\n";


