<?php
include('config.php');
include(DBFILE);
include('login.php');

if (!$authenticated)
{

$email = strtolower(trim(substr(filter_var($_POST['username'],FILTER_VALIDATE_EMAIL),0,128)));
$pwd = trim(substr(filter_var($_POST['pwd'],FILTER_SANITIZE_STRING),0,64));
$sql = "SELECT * FROM accnt WHERE email='".
		mysqli_real_escape_string($conn,$email)."' AND activate_code=''";

$res = mysqli_query($conn,$sql);
if (mysqli_num_rows($res)>0)
{
	$row = mysqli_fetch_array($res);
	$chkpwd = $row['pwd'];
	if (password_verify($pwd,$chkpwd))
	{
		$sql = "INSERT INTO logins (idx,accountidx,is_paypal,sessionid,ip,sequence,".
			"logout) ".
            "VALUES (NULL,'".
            mysqli_real_escape_string($conn,$row['idx'])."','N','".
            mysqli_real_escape_string($conn,$sessionid)."','".
            mysqli_real_escape_string($conn,$_SERVER['REMOTE_ADDR'])."','".
            time()."',0)";
            mysqli_query($conn,$sql);
            mysqli_free_result($res);
	}
}
mysqli_free_result($res);
}
mysqli_close($conn);
Header("Location: index.php");
exit();

