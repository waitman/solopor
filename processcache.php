<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

require('HTTPSig.php');
include('config.php');
include(DBFILE);

$keys = json_decode(file_get_contents(KEYPATH),true);

$p = json_decode(file_get_contents(ACTORPATH),true);
$actor = $p['url'];


$sql = "SELECT * FROM inbox WHERE cache!='Y' AND content LIKE '%\"type\":\"Announce\"%'";
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$sql = "UPDATE inbox SET cache='Y' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."'";
	mysqli_query($conn,$sql);
	$content = json_decode($row['content'],true);
	$object = $content['object'];


        $tg=explode('/',$object);
        array_shift($tg);
        array_shift($tg);
        array_shift($tg);
        $target = '/'.join('/',$tg);

        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('UTC'));
        $headers = array();
        $headers['Accept'] = ACTIVITY_HEADER;
        $headers['Date'] = str_replace('+0000','GMT',$date->format(DateTimeInterface::RFC1123));
        $headers['Digest'] = HTTPSig::generate_digest_header($ret);
        $headers['(request-target)'] = 'get '.$target;
        $xhead = HTTPSig::create_sig($headers,$keys['prvkey'],$actor);

	$ch = curl_init($object);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $xhead);
        $result = curl_exec($ch);
	$sql = "INSERT INTO announcement_cache (idx,request_idx,item_id,content,created,updated) VALUES (NULL,'".
		mysqli_real_escape_string($conn,$row['idx'])."','".
		mysqli_real_escape_string($conn,$object)."','".
		mysqli_real_escape_string($conn,$result)."','".time()."','0')";
	mysqli_query($conn,$sql);
}
mysqli_free_result($res);
mysqli_close($conn);

