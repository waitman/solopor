<?php

function check_siteallowed($v) {}
function logger($v) {}

function random_string($size = 64, $type = RANDOM_STRING_HEX) {
	$s = hash('whirlpool', (string) rand() . uniqid(rand(),true) . (string) rand(),(($type == RANDOM_STRING_TEXT) ? true : false));
	$s = (($type == RANDOM_STRING_TEXT) ? str_replace("\n","",base64url_encode($s,true)) : $s);
	return(substr($s, 0, $size));
}

function base64url_encode($s, $strip_padding = true) {
 
        $s = strtr(base64_encode($s),'+/','-_');
 
        if($strip_padding)
                $s = str_replace('=','',$s);
 
        return $s;
}
        
function base64url_decode($s) {
        if(is_array($s)) {
                logger('base64url_decode: illegal input: ' . print_r(debug_backtrace(), true));
                return $s;
        }
        return base64_decode(strtr($s,'-_','+/'));
}
    


