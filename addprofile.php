<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$addr = $argv[1];

$photo = 'N';
$private = 'N';
$shop = 'N';

if ($argv[2]=='photo') 
{
	$photo = 'Y';
	$private = 'Y';
}

if ($addr=='') exit("\n".'no address'."\n");

$sql = "SELECT * FROM profiles WHERE url='".
	mysqli_real_escape_string($conn,$addr)."'";
$res = mysqli_query($conn,$sql);
if (mysqli_num_rows($res)>0)
{
	mysqli_free_result($res);
	echo "\nError - Profile exists\n\n";
	exit();
}
mysqli_free_result($res);


$ch = curl_init($addr);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.LDJSON_HEADER));
$result = curl_exec($ch);

 
$sql = "INSERT INTO profiles (idx,url,created,approved,pending,following,follower,updated,deleted,private,shop,photo,content,request) VALUES (NULL,'".
	mysqli_real_escape_string($conn,$addr)."','".
	mysqli_real_escape_string($conn,time())."','0','0','N','N','0','0','".
	mysqli_real_escape_string($conn,$private)."','".
	mysqli_real_escape_string($conn,$shop)."','".
	mysqli_real_escape_string($conn,$photo)."','".
	mysqli_real_escape_string($conn,$result)."','')";
mysqli_query($conn,$sql) or die($sql);
mysqli_close($conn);

echo "\n".'OK'."\n\n";

