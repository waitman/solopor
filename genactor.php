<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');

$keys = json_decode(file_get_contents(KEYPATH),true);

$ret = array();


$ret['@context'] = [ 'https://www.w3.org/ns/activitystreams','https://w3id.org/security/v1' ];
$ret['type']				= 'Person';
$ret['id']				= 'https://solopor.com/wago/';
$ret['preferredUsername']		= 'wago';
$ret['name']				= 'Waitman Gobble';
$ret['updated']				= date('c');
$ret['icon']				= [
						'type'			=> 'Image',
						'mediaType'		=> 'image/jpeg',
						'updated'		=> date('c'),
						'url'			=> 'https://solopor.com/SOLOPOR/moibh800.jpg',
						'height'		=> 800,
						'width'			=> 800
					];
$ret['image']				= [
						'type' 			=> 'Image',
						'mediaType' 	=> 'image/png',
						'url' 			=> 'https://solopor.com/ui/Landschaft_87.png',
						'height'		=> 700,
						'width'			=> 350	
					];
$ret['url']				= 'https://solopor.com/wago/';
$ret['inbox']				= 'https://solopor.com/inbox/wago';
$ret['outbox']				= 'https://solopor.com/outbox/wago';
$ret['followers']			= 'https://solopor.com/followers/wago';
$ret['following']			= 'https://solopor.com/following/wago';
$ret['endpoints']			= [ 'sharedInbox' => 'https://solopor.com/inbox' ];
$ret['discoverable']			= 1;
$ret['publicKey']			= [
						'id'			=> 'https://solopor.com/wago/#mainkey',
						'owner'			=> 'https://solopor.com/wago/',
						'publicKeyPem'	=> $keys['pubkey']
					];
$ret['summary']				= 'I am building an ActivityPub site from scratch. Not much here yet. Code on <a href="https://framagit.org/waitman/solopor">git</a>.';

file_put_contents(ACTORPATH,json_encode($ret));

echo "\n\n".'Actor file written to '.ACTORPATH."\n\n";


$wf = array();
$wf['subject'] = 'acct:wago@solopor.com';
$wf['aliases'] = [ 'https://solopor.com/wago', 'https://solopor.com/@wago', 'https://solorpor.com/users/wago' ];
$wf['properties'] = [
		'http://webfinger.net/ns/name' => $ret['name'],
		'http://xmlns.com/foaf/0.1/name' => $ret['name'],
		'https://w3id.org/security/v1#publicKeyPem' => $keys['pubkey'],
		'http://purl.org/zot/federation' => 'activitypub'
	];
$wf['links'] = [
		0 => [
			'rel' => 'http://webfinger.net/rel/avatar',
            		'type' => 'image/jpeg',
			'href' => 'https://solopor.com/SOLOPOR/moibh800.jpg'
		],
		1 => [
			'rel' => 'http://webfinger.net/rel/blog',
			'href' => $ret['id']
		],
		2 => [
			'rel' => 'self',
			'type' => 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
			'href' => $ret['id']
		],
		3 => [
			'rel' => 'self',
			'type' => 'application/activity+json',
			'href' => $ret['id']
		],
		4 => [
			'rel' => 'http://ostatus.org/schema/1.0/subscribe',
			'template' => 'https://solopor.com/follow/{uri}'
		]
	];
                

file_put_contents(WEBFINGERPATH,json_encode($wf));

echo "\n\n".'Web Finger file written to '.WEBFINGERPATH."\n\n";


