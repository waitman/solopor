<?php

barf();

if ((count($page)==3)&&(strlen($page[2])>=32))
{
	$sql = "SELECT * FROM outbox WHERE item_id LIKE '%/".
		mysqli_real_escape_string($conn,$page[2])."' AND is_deleted!='Y'";
	$res = mysqli_query($conn,$sql);
	if (mysqli_num_rows($res)<1)
	{
		$content = '<h1>OOPS</h1><p>'.htmlentities($page[2]).' does not exist on this server.</p>';
	} else {
		
		//OUTPUT
		$row = mysqli_fetch_array($res);
		$object = json_decode($row['object'],true);
		$actor = json_decode(file_get_contents(ACTORPATH),true);
		$published = date('n/d/Y g:i a e',$row['published']);
		$content = '<h1>'.htmlentities($row['item_type']).'</h1>
<div>
<a href="'.$actor['url'].'"><img src="'.$actor['icon']['url'].'" style="width:100px;height:auto;border-radius:5px;"></a> 
<a href="'.$actor['url'].'">'.htmlentities($actor['name']).'</a>
</div>
';
		$sql = "SELECT content FROM object_cache WHERE request_idx='".
			mysqli_real_escape_string($conn,$row['idx'])."'";
		$xres = mysqli_query($conn,$sql);
		$xrow = mysqli_fetch_array($xres);
		$xc = json_decode($xrow['content'],true);
		mysqli_free_result($xres);

		$details=array();
		$details['icon']['url']='/SOLOPOR/nobody.png';
		$details['url']='#';
		$details['name']='nobody';

		$sql = "SELECT content FROM profiles WHERE content LIKE '%".
			mysqli_real_escape_string($conn,$xc['attributedTo'])."%'";
		$jres = mysqli_query($conn,$sql);
		if (mysqli_num_rows($jres)>0)
		{
			$jrow = mysqli_fetch_array($jres,MYSQLI_ASSOC);
			$details = json_decode($jrow['content'],true);
		}
		mysqli_free_result($jres);

		$attach = $xc['attachment'];
		$attachments = array();

		if (is_array($attach))
		{
			foreach ($attach as $k=>$v)
			{
				switch ($v['mediaType'])
				{
					case 'image/jpeg': /* fall-through */
					case 'image/png': /* fall-through */
					case 'image/gif':
						$attachments[]='<div style="padding:5px;"><img src="'.$v['url'].'" style="max-width:100%;height:auto;"></div>';
						break;
					default:
						$attachments[]='<div style="padding:5px;"><a href="'.$v['url'].'" class="text-danger">'.htmlentities($v['url']).'</a></div>';
						break;
				}
			}
		}

		if ($authenticated)
		{
			$share = '[<a href="/share/'.$row['inbox_idx'].'" class="text-warning">re-share</a>]';
		}
		$content .= '
<script>
function show(v)
{
	$("#m"+v).toggle();
}
</script>
<div class="pt-3">
<table class="table table-dark table-striped table-sm">
<tbody class="text-light">
<tr>
<td>type:</td><td><b>----&gt; '.htmlentities($xc['type']).' &lt; ----</b></td>
</tr>
<tr>
<td>content id:</td><td><a href="'.$row['item_id'].'" class="text-light">'.$row['item_id'].'</a> '.$share.'</td>
</tr>
<tr>
<td>shared:</td><td>'.$published.'</td>
</tr>
<tr>
<td>source id:</td><td><a href="'.$xc['id'].'" class="text-light">'.htmlentities($xc['id']).'</a></td>
</tr>
<tr>
<td>pub date:</td><td>'.htmlentities($xc['published']).'</td>
</tr>
<tr>
<td>inReplyTo:</td><td><a href="'.$xc['inReplyTo'].'" class="text-light">'.htmlentities($xc['inReplyTo']).'</a></td>
</tr>
<tr>
<td>attributed to</td>
<td>
<a href="'.$details['url'].'"><img src="'.$details['icon']['url'].'" style="width:100px;height:auto;border-radius:5px;"></a>
<a href="'.$details['url'].'" class="text-warning"><b>'.htmlentities($details['name']).'</b></a><br>
<a href="'.$xc['attributedTo'].'" class="text-warning">'.$xc['attributedTo'].'</a>
</td>
</tr>
<tr>
<td>content</td>
<td>'.$xc['content'].'</td>
</tr>
<tr>
<td>attachments</td><td>'.join("\n",$attachments).'</td>
</tr>
</tbody>
</table>
</div>
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($xc,true).'</pre>
</div>
';
		mysqli_free_result($xres);
	}
	mysqli_free_result($res);
} else {
	$content = '<h1>OOPS</h1><p>Error!</p>';
}

