<?php

barf();

if ($authenticated)
{
	$page = intval($page[2]);
	$start = $page*99;
	$sql = "SELECT COUNT(idx) FROM gallery WHERE is_delete!='Y'";
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	$num_pages = ceil($row[0]/99);
	$num_images = $row[0];
	mysqli_free_result($res);

	$ops = array();
	for ($i=0;$i<$num_pages;$i++)
	{
		$sel = '';
		if ($page==$i) $sel = ' selected';
		$ops[]='<option value="'.$i.'"'.$sel.'>'.($i+1).'</option>';
	}
				
	$content = '<h1>Gallery '.$num_images.'</h1>
<script>
function changesel()
{
	var selv = $("#dulco").children("option:selected").val();
	window.location= "/gallery/" + selv;
}
</script>
<div class="p-3">
<select id="dulco" onchange="changesel();">'.join('',$ops).'</select>
</div>
<div class="row">
';
	$c=0;
	$sql = "SELECT * FROM gallery ORDER BY idx DESC LIMIT ".$start.",99";
	$res = mysqli_query($conn,$sql);
	while ($row = mysqli_fetch_array($res))
	{
		$content .= '
<div class="col text-center">
<img src="/photos/th/'.$row['dt'].'/'.$row['image'].'.jpg" class="img-fluid" alt="'.$row['idx'].':'.$row['inbox_idx'].'">
</div>
';
		$c++;
		if ($c>2)
		{
			$c=0;
			$content .= '
</div>
<div class="row">
';
		}
	}
	mysqli_free_result($res);
	$content .= '
</div>';
}

