<?php

barf();
 
if ($authenticated)
{
	$id = ers($page[2],64);
	if ($id!='' && is_array($_FILES) && array_key_exists('file',$_FILES))
	{
		$name = basename($_FILES['file']['name']);
		$ext = strtolower(pathinfo($name,PATHINFO_EXTENSION));
		$save_name = bin2hex(random_bytes(32)).'.'.$ext;
		$dt = date('Y-m-d');
		if (!is_dir(WWWPATH.'/st/'.$dt))
		{
			mkdir('st/'.$dt);
		}
		move_uploaded_file($_FILES['file']['tmp_name'],WWWPATH.'/st/'.$dt.'/'.$save_name);
		if (file_exists(WWWPATH.'/st/'.$dt.'/'.$save_name))
		{
			$mime_type = mime_content_type(WWWPATH.'/st/'.$dt.'/'.$save_name);
			$type = 'unknown';
			if (strstr($mime_type,'video'))
			{
				$type='video';
			}
			if (strstr($mime_type,'image'))
			{
				$type='image';
			}
			if (strstr($mime_type,'audio'))
			{
				$type='audio';
			}
			$sql = "INSERT INTO attachments (idx,comp_id,dt,type,mediaType,filename,name,is_deleted,sequence) VALUES (NULL,'".
				mysqli_real_escape_string($conn,$id)."','".
				mysqli_real_escape_string($conn,$dt)."','".
				mysqli_real_escape_string($conn,$type)."','".
				mysqli_real_escape_string($conn,$mime_type)."','".
				mysqli_real_escape_string($conn,$save_name)."','".
				mysqli_real_escape_string($conn,$name)."','N','".time()."')";
				mysqli_query($conn,$sql);
		}
	}
}
mysqli_close($conn);


