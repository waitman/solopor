<?php

barf();

$v = `wc -l .git/logs/HEAD`;
$x = explode(' ',trim($v));
$version = '0.'.array_shift($x);
$f=file_get_contents('instance.json');

$sql = "SELECT COUNT(idx) FROM profiles WHERE following='Y' AND deleted=0";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);
$following = intval($row[0]);
mysqli_free_result($res);

$sql = "SELECT COUNT(idx) FROM profiles WHERE follower='Y' AND deleted=0";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);
$followers = intval($row[0]);
mysqli_free_result($res);

$sql = "SELECT COUNT(idx),MAX(published) FROM outbox WHERE is_deleted!='Y'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);
$cnt = intval($row[0]);
$last = date('Y-m-d',$row[1]);
mysqli_free_result($res);

$f=str_replace('<!--VERSION-->',$version,$f);
$f=str_replace('<!--FOLLOWERS-->',$followers,$f);
$f=str_replace('<!--FOLLOWING-->',$following,$f);
$f=str_replace('<!--OUTBOX COUNT-->',$cnt,$f);
$f=str_replace('<!--LAST STATUS-->',$last,$f);

mysqli_close($conn);
$out = json_encode(json_decode($f,true));
Header("Content-type: application/json; charset=utf-8");
Header("Content-length: ".strlen($out));
echo $out;

