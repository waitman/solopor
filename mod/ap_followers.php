<?php

barf();

$ovr=true;
$sql = "SELECT COUNT(idx) FROM profiles WHERE follower='Y'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);
$cnt = $row[0];
$pgs = ceil($cnt/12);
mysqli_free_result($res);

if (count($page)>3)
{
	$cc=array();
	$pg = intval($page[3]);
	if (($pg>0)&&($pg<=$pgs))
	{
		$ovr = false;
		$sql = "SELECT * FROM profiles WHERE follower='Y' ORDER BY idx DESC LIMIT ".($pg-1).",12";
		$res = mysqli_query($conn,$sql);
		while ($row = mysqli_fetch_array($res))
		{
			$co = json_decode($row['content'],true);
			$cc[]=$co['url'];
		}
		mysqli_free_result($res);

		$o = array();
		$o['@context']='https://www.w3.org/ns/activitystreams';
		$o['id']='https://'.DOMAIN.'/followers/'.USER.'/'.$pg;
		$o['type']='OrderedCollectionPage';
		$o['totalItems']=$cnt;
		if ($pgs>$pg) $o['next']='https://'.DOMAIN.'/followers/'.USER.'/'.($pg+1);
		$o['partOf']='https://'.DOMAIN.'/followers/'.USER;
		$o['orderItems']=$cc;
	}
}

if ($ovr)
{
	$o=array();
	$o['@context']='https://www.w3.org/ns/activitystreams';
	$o['id']='https://'.DOMAIN.'/followers/'.USER;
	$o['type']='OrderedCollection';
	$o['totalItems']=$cnt;  
	$o['first']='https://'.DOMAIN.'/followers/'.USER.'/1';
}

$out = json_encode($o,JSON_UNESCAPED_SLASHES);
Header('Content-type: application/ld+json; profile="https://www.w3.org/ns/activitystreams');
Header('Content-length: '.strlen($out));
echo $out;

