<?php

barf();

if ($_SERVER['REQUEST_METHOD']=='POST')
{
	$data = file_get_contents('php://input');
	$sql = "INSERT INTO inbox (idx,user,ip,created,seen,processed,errors,headers,content) VALUES (NULL,'".
		mysqli_real_escape_string($conn,$page[2])."','".
		mysqli_real_escape_string($conn,$_SERVER['REMOTE_ADDR'])."','".
		mysqli_real_escape_string($conn,time())."','0','0','N','".
		mysqli_real_escape_string($conn,json_encode($_SERVER))."','".
		mysqli_real_escape_string($conn,$data)."')";
	mysqli_query($conn,$sql);
	exit();
} else {

	if ($authenticated)
	{
		$return_to = 'inbox';
		if ($private) $return_to = 'private';
		$content = '<h1>Inbox</h1>
<script>
function changesel()
{
	var selv = $("#dulco").children("option:selected").val();
	window.location= "/'.$return_to.'/" + selv;
}
</script>
<style>
@media screen and (max-width: 480px) {
.inbox-msg { border-top:2px solid #fff; font-size: 2.6vw;  overflow:hidden; }
}
@media screen and (min-width: 481px) {
.inbox-msg { border-top:2px solid #fff; font-size: 0.8vw; overflow:hidden; }
}
.inbox-headline { padding:3px; color:#000; background-color: rgba(255, 255, 255, 0.6); }
.inbox-headline-note { padding:3px; color:#fff; background-color: rgba(21, 55, 241, 0.2); }
.inbox-headline-announcement { padding:3px; color:#000; background-color: rgba(154, 243, 255, 0.6); }
.inbox-headline-like { padding:3px; color:#000; background-color: rgba(154, 255, 154, 0.6); }
.inbox-headline-dim { padding:3px; color:#000; background-color: rgba(255, 255, 255, 0.2); }
.inbox-head { width: 60px; border-radius: 5px; margin:3px; }
.tt { color: #000; }
.inbox-body { padding:5px; }
</style>
<script>
function show(v)
{
	$("#m"+v).toggle();
}
</script>
';

		$cutoff = strtotime('-180 days');
		$offset = (4*60*60);
		$sql = "SELECT FROM_UNIXTIME(created-".$offset.", '%Y-%m-%d') AS cd,".
		"COUNT(idx) FROM inbox WHERE created>='".$cutoff."' GROUP BY cd ORDER BY created DESC";
		$res = mysqli_query($conn,$sql);
		$chkdates=array();
		$date_options=array();
		while ($row = mysqli_fetch_array($res))
		{
			$chkdates[$row[0]]=true;
			$date_options[]='<option value="'.strtotime($row[0].' 00:00:00').'">'.$row[0].' ('.$row[1].')</option>';
		}
		$dto=join('',$date_options);

		$dt = intval($page[2]);
		if ($dt<1589688000) 
		{
			$dt = strtotime('-8 hours');
			$et = time();
		} else {
			$et = $dt + 24 * 60 * 60;
		}
					
		$dto = str_replace('"'.$dt.'"','"'.$dt.'" selected="selected"',$dto);
		$content .= '<div class="pb-3"><select id="dulco" onchange="changesel();">'.$dto.'</select></div>';

		if (!$private)
		{
			$content .= '<div class="pb-3"><a href="/private">Private</a> &middot; 
<a href="/gallery">Gallery</a> &middot; <a href="/mobilegallery">Mobile Gallery</a></div>';
		} else {
			$content .= '<div class="pb-3"><a href="/inbox">Return to Inbox</a> &middot; <a href="/gallery">Gallery</a> &middot; 
<a href="/mobilegallery">Mobile Gallery</a></div>';
		}

		$sql = "SELECT * FROM inbox WHERE created>=".$dt." AND created<".$et." AND post_process!='Y' ORDER BY idx DESC";
		$res = mysqli_query($conn,$sql);
		while ($row = mysqli_fetch_array($res))
		{
			$msg = json_decode($row['content'],true);
			switch ($msg['type'])
			{
				case ACTIVITY_DISLIKE: /*fall-through*/
				case ACTIVITY_LIKE:
					$details=array();
					$details['icon']['url']='/SOLOPOR/nobody.png';
					$details['url']='#';
					$details['name']='nobody';
					if (array_key_exists('actor',$msg) && ($msg['actor']!=''))
					{
						$details['url']=$msg['actor'];
						$details['name']=$msg['actor'];
						$sql = "SELECT content FROM profiles WHERE content LIKE '%".
						mysqli_real_escape_string($conn,$msg['actor'])."%'";
						$xres = mysqli_query($conn,$sql);
						if (mysqli_num_rows($xres)>0)
						{
							$xrow = mysqli_fetch_array($xres,MYSQLI_ASSOC);
							$details = json_decode($xrow['content'],true);
						}
						mysqli_free_result($xres);
					}
					$share = '<a href="/share/'.$row['idx'].'" class="tt">share</a>';
					$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="inbox-msg">
<div class="inbox-headline-like">
<div>
<a href="'.$details['url'].'"><img class="inbox-head" src="'.$details['icon']['url'].'"></a>
<a href="'.$details['url'].'" class="tt">'.htmlentities($details['name']).'</a>
</div>
<div>
<div style="width:50px;float:right;padding:5px;text-align:right;" class="small">'.$share.'</div>
<b>----&gt; '.$msg['type'].' &lt;----</b><br>
<b>url:</b> <a class="tt" href="'.$msg['inReplyTo'].'">'.htmlentities($msg['inReplyTo']).'</a><br>
<b>date:</b> '.$msg['published'].'<br>
</div>
</div>
<div class="inbox-body">
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
</div>
</div>
';
					break;

				case ACTIVITY_SHARE:
					$is_private='N';
					$details=array();
					$details['icon']['url']='/SOLOPOR/nobody.png';
					$details['url']='#';
					$details['name']='nobody';

					if (array_key_exists('actor',$msg) && ($msg['actor']!=''))
					{
						$details['url']=$msg['actor'];
						$details['name']=$msg['actor'];
						$sql = "SELECT content,private FROM profiles WHERE content LIKE '%".
							mysqli_real_escape_string($conn,$msg['actor'])."%'";
						$xres = mysqli_query($conn,$sql);
						if (mysqli_num_rows($xres)>0)
						{
							$xrow = mysqli_fetch_array($xres,MYSQLI_ASSOC);
							$is_private = $xrow['private'];
							$details = json_decode($xrow['content'],true);
						}
						mysqli_free_result($xres);

						$load_cache = false;
						if ($is_private=='Y')
						{
							if ($private)
							{
								$load_cache = true;
							}
						} else {
							$load_cache = true;
						}
						unset($xc);
						unset($attachments);
						unset($p_details);
						if ($load_cache)
						{
							$sql = "SELECT content FROM announcement_cache WHERE request_idx='".
								mysqli_real_escape_string($conn,$row['idx'])."'";
							$xres = mysqli_query($conn,$sql);
							$xrow = mysqli_fetch_array($xres);
							$xc = json_decode($xrow['content'],true);
							mysqli_free_result($xres);
						}

						if (is_array($xc) && (!array_key_exists('error',$xc)))
						{
							$p_details=array();
							$p_details['icon']['url']='/SOLOPOR/nobody.png';
							$p_details['url']=$xc['attributedTo'];
							$p_details['name']=$xc['attributedTo'];
							$sql = "SELECT content FROM profiles WHERE content LIKE '%".
								mysqli_real_escape_string($conn,$xc['attributedTo'])."%'";
							$jres = mysqli_query($conn,$sql);
							if (mysqli_num_rows($jres)>0)
							{
								$jrow = mysqli_fetch_array($jres,MYSQLI_ASSOC);
								$p_details = json_decode($jrow['content'],true);
							}
							mysqli_free_result($jres);
							$attach = $xc['attachment'];
							$attachments = array();
							if (is_array($attach))
							{
								foreach ($attach as $k=>$v)
								{
									switch ($v['mediaType'])
									{
										case 'image/jpeg': /* fall-through */
										case 'image/png': /* fall-through */
										case 'image/gif':
											$attachments[]='
<div style="padding:5px;"><img src="'.$v['url'].'" style="max-width:100%;height:auto;"></div>
';
											break;
										default:
											$attachments[]='
<div style="padding:5px;"><a href="'.$v['url'].'" class="text-danger">'.htmlentities($v['url']).'</a></div>
';
											break;
									}
								}
							}
						}
					}
					$share = '<a href="/share/'.$row['idx'].'" class="tt">share</a>';
					if ($load_cache)
					{
						$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="inbox-msg">
<div class="inbox-headline-announcement">
<div>
<a href="'.$details['url'].'"><img class="inbox-head" src="'.$details['icon']['url'].'"></a>
<a href="'.$details['url'].'" class="tt">'.htmlentities($details['name']).'</a>
</div>
<div>
<div style="width:50px;float:right;padding:5px;text-align:right;" class="small">'.$share.'</div>
<b>----&gt; Announcement &lt;----</b><br>
<b>url:</b> <a class="tt" href="'.$msg['object'].'">'.htmlentities($msg['object']).'</a><br>
<b>date:</b> '.$msg['published'].'<br>
</div>
</div>
<div class="inbox-body">
<div style="background-color:#ccc;">
<a href="'.$p_details['url'].'"><img class="inbox-head" src="'.$p_details['icon']['url'].'"></a>
<a href="'.$p_details['url'].'" class="tt">'.htmlentities($p_details['name']).'</a>
</div>
'.$xc['content'].'
'.join("\n",$attachments).'
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
</div>
</div>
';
					}     
					break;
				// informational only, check source is available
				case ACTIVITY_REJECT:	/* fall-through */
				case ACTIVITY_DELETE:	/* fall-through */
				case ACTIVITY_ACCEPT:	/* fall-through */
				case ACTIVITY_FOLLOW:	/* fall-through */
				case ACTIVITY_UNDO:
					$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="inbox-message">
<div class="inbox-headline-dim text-right">(x) '.$msg['type'].'</div>
<div class="inbox-body">
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
</div>
';
					break;
				case ACTIVITY_UPDATE: 	/* fall-through */
				case ACTIVITY_CREATE:
					$is_private='';
					$details=array();
					$details['icon']['url']='/SOLOPOR/nobody.png';
					$details['url']='#';
					$details['name']='nobody';
					if (array_key_exists('actor',$msg) && ($msg['actor']!=''))
					{
						$details['url']=$msg['actor'];
						$details['name']=$msg['actor'];
						$sql = "SELECT content,private FROM profiles WHERE content LIKE '%".
							mysqli_real_escape_string($conn,$msg['actor'])."%'";
						$xres = mysqli_query($conn,$sql);
						if (mysqli_num_rows($xres)>0)
						{
							$xrow = mysqli_fetch_array($xres,MYSQLI_ASSOC);
							$is_private = $xrow['private'];
							$details = json_decode($xrow['content'],true);
						}
						mysqli_free_result($xres);
					}
					$attach = $msg['object']['attachment'];
					$attachments = array();

					if (is_array($attach))
					{
						foreach ($attach as $k=>$v)
						{
							switch ($v['mediaType'])
							{
								case 'image/jpeg': /* fall-through */
								case 'image/png': /* fall-through */
								case 'image/gif': 
									$attachments[]='<div style="padding:5px;"><img src="'.$v['url'].'" style="max-width:100%;height:auto;"></div>';
									break;
								default:
									$attachments[]='<div style="padding:5px;"><a href="'.$v['url'].'">'.htmlentities($v['url']).'</a></div>';
									break;
							}
						}
					}
					$share = '<a href="/share/'.$row['idx'].'" class="text-light">share</a>';
					$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="inbox-msg">
<div class="inbox-headline-note">
<div>
<a href="'.$details['url'].'"><img class="inbox-head" src="'.$details['icon']['url'].'"></a>
<a href="'.$details['url'].'" class="tt text-light">'.htmlentities($details['name']).'</a>
</div>
<div>
<div style="width:50px;float:right;padding:5px;text-align:right;" class="small">'.$share.'</div>
<b>----&gt; '.$msg['object']['type'].' &lt;----</b><br>
<b>url:</b> <a class="tt text-light" href="'.$msg['object']['url'].'">'.htmlentities($msg['object']['url']).'</a><br>
<b>replyto:</b> <a class="tt text-light" href="'.$msg['object']['inReplyTo'].'">'.htmlentities($msg['object']['inReplyTo']).'</a><br>
<b>date:</b> '.$msg['published'].'<br>
<b>private:</b> '.$is_private.'<br>
</div>
</div>
<div class="inbox-body">
';

					if ($is_private=='Y')
					{
						if ($private && $authenticated)
						{
							$content .= '
'.$msg['object']['content'].'
'.join("\n",$attachments);
						}
					} else {
						$content .= '
'.$msg['object']['content'].'
'.join("\n",$attachments).'
';
					}
					$content .= '
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
</div>
</div>
';
					break;
				default: 
					$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="inbox-msg">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
';
					break;

/*
define ('ACTIVITY_CREATE',      'Create' );
define ('ACTIVITY_UPDATE',      'Update' );
define ('ACTIVITY_LIKE',        'Like' );
define ('ACTIVITY_DISLIKE',     'Dislike' );
define ('ACTIVITY_SHARE',       'Announce' );
define ('ACTIVITY_FOLLOW',      'Follow' );
define ('ACTIVITY_UNFOLLOW',    'Unfollow');
define ('ACTIVITY_ACCEPT',      'Accept');
*/
			}
		}
		mysqli_free_result($res);
	} else {
		$content = '<h1>Inbox</h1><p>Please <a href="/login">Login</a>.</p>';
	}
}


