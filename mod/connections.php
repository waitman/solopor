<?php

barf();

$sql = "SELECT * FROM profiles WHERE (following='Y' OR follower='Y' OR pending>0) AND deleted<1 ORDER BY idx DESC";
$res = mysqli_query($conn,$sql);

$content = '
<h1>Connections</h1>
<!--Approvals-->
<style>
@media screen and (max-width: 480px) {
.tt { font-size: 2.6vw; color:#000; font-weight: bold; }
}
@media screen and (min-width: 481px) {
.tt { font-size: 0.8vw; color:#000; }
}
</style>
<div class="row">
';
$approvals=array();
$c=0;
while ($row = mysqli_fetch_array($res))
{
	$details = json_decode($row['content'],true);
	$follow = '';
	if ($authenticated)
	{
		if ($row['pending']>0)
		{
			$approvals[]='<div><a href="'.$details['url'].'"><img style="width:300px;max-width:50%;height:auto;" src="'.$details['icon']['url'].'"></a> '.htmlentities($details['name']).'<br><span class="small">Pending Follow Request<br><a href="/authorize/'.$row['idx'].'" class="btn btn-sm btn-primary">Authorize</a> <a href="/cancel/'.$row['idx'].'" class="btn btn-sm btn-danger">Cancel</a></span></div>';
		}
	}
	if (($row['follower']=='Y') || ($row['following']=='Y'))
	{
		$show = true;
		if ($row['private']=='Y')
		{
			if (!$authenticated) $show=false;
		}
		if ($show)
		{
			$c++;
			if ($c>3)
			{
				$c=1;
				$content .= '</div>
<div class="row">
';
			}
			$content .= '
<style>
#xr'.$row['idx'].' { background-image: url('.$details['image']['url'].'); background-repeat: no-repeat; border-radius:7px; background-size: cover; margin:5px; height:150px; padding:0 !important; }
#ixr'.$row['idx'].' { width:60px; height:auto; border-radius:5px; padding-top:30px; }
#mxr'.$row['idx'].' { height: 90px; overflow:hidden; }
#txr'.$row['idx'].' { border-radius:0 0 7px 7px; height: 30px; margin-top:30px; padding-top: 3px; border-top:1px solid #111; background-color: rgba(255, 255, 255, 0.6);  }
</style>
<div class="col text-center" id="xr'.$row['idx'].'">
<div id="mxr'.$row['idx'].'"><a href="'.$details['url'].'">
<img id="ixr'.$row['idx'].'" src="'.$details['icon']['url'].'">
</a>
</div>
<div id="txr'.$row['idx'].'"><a href="'.$details['url'].'" class="tt">'.htmlentities($details['name']).'</a></div>
</div>
';
		}
	}
}

for ($i=$c;$i<3;$i++)
{
	$content .= '
<style>
#xd'.$i.' { border-radius:7px; margin:5px; height:150px; padding:0 !important; }
</style>
<div id="xd'.$i.'" class="col"> </div>
';
}

$content .= '</div>
';

$content = str_replace('<!--Approvals-->',join('',$approvals),$content);
mysqli_free_result($res);

