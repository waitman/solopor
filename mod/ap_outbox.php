<?php;

barf();

$per_page = 20;
$sql = "SELECT COUNT(idx) FROM outbox WHERE is_deleted!='Y'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);
$total_items = $row[0];
mysqli_free_result($res);
$pages = ceil($total_items/$per_page);
$ob = 'https://'.DOMAIN.'/outbox';
if ($path[3]==USER) $ob ='https://'.DOMAIN.'/outbox/'.USER;

$this_page = array_pop($page);
if (is_numeric($this_page) && intval($this_page)>0)
{
	$this_page = intval($this_page);
	if ($this_page>$pages) $this_page = 0;
} else {
	$this_page = 0;
}

if ($this_page<1)
{
	$a=array();
	$a['@context']='https://www.w3.org/ns/activitystreams';
	$a['id'] = $ob;
	$a['type'] = 'OrderedCollection';
	$a['totalItems'] = $total_items;
	$a['first'] = $ob.'/1';
	$a['last']= $ob.'/'.$pages;
} else {
	$a=array();
	$a['@context']='https://www.w3.org/ns/activitystreams';
	$a['id']=$ob.'/'.$this_page;
	$a['type'] = 'OrderedCollection';
	if ($this_page<$pages)
	{
		$a['next']=$ob.'/'.($this_page+1);
	}
	if ($this_page>1)
	{
		$a['prev']=$ob.'/'.($this_page-1);
	}
	$a['orderedItems'] = array();
	$start = $this_page-1;
	$sql = "SELECT * FROM outbox WHERE is_deleted!='Y' ORDER BY idx DESC LIMIT ".$start.",".$per_page;
	$res = mysqli_query($conn,$sql);
	while ($row = mysqli_fetch_array($res))
	{
		$object = json_decode($row['object'],true);
		unset($object['content']);
		unset($object['signature']);
		$a['orderedItems'][]=$object;
	}
	mysqli_free_result($res);
}

$out = json_encode($a,JSON_UNESCAPED_SLASHES);
Header('Content-type: application/ld+json; profile="https://www.w3.org/ns/activitystreams');
Header('Content-length: '.strlen($out));
echo $out;

