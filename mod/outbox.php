<?php

barf();

$content = '
<h1>Outbox</h1>
<style>
@media screen and (max-width: 480px) {
.outbox-msg { border-top:2px solid #fff; font-size: 2.6vw;  overflow:hidden; }
}
@media screen and (min-width: 481px) {
.outbox-msg { border-top:2px solid #fff; font-size: 0.8vw; overflow:hidden; }
}
.outbox-headline { padding:3px; color:#000; background-color: rgba(255, 255, 255, 0.6); }
.outbox-headline-note { padding:3px; color:#fff; background-color: rgba(21, 55, 241, 0.2); }
.outbox-headline-announcement { padding:3px; color:#000; background-color: rgba(154, 243, 255, 0.6); }
.outbox-headline-like { padding:3px; color:#000; background-color: rgba(154, 255, 154, 0.6); }
.outbox-headline-dim { padding:3px; color:#000; background-color: rgba(255, 255, 255, 0.2); }
.outbox-head { width: 60px; border-radius: 5px; margin:3px; }
.tt { color: #000; }
.outbox-body { padding:5px; }
</style>
<script>
function show(v)
{
	$("#m"+v).toggle();
}
</script>
';



if ($authenticated)
{
	$content .= '
<script>
function emot(v)
{
 var cn = $("#nda").val();
 $("#nda").val(cn+v);
}
</script>
<link href="./dropzone-5.7.0/dist/min/basic.min.css" rel="stylesheet">
<script src="./dropzone-5.7.0/dist/min/dropzone.min.js"></script>
<style>
.bot { width:46px;height:46px;font-size:20px; }
.scrl { height:130px; overflow-y: scroll; }
</style>
<div class="scrl">
';
	for ($i=0x1F601;$i<=0x1F64F;$i++)
	{
		$content .= '<button class="btn btn-dark bot" onclick="emot('."'&#x".dechex($i)."'".');">&#x'.dechex($i).'</button>';
	}
	for ($i=0x1F600;$i<=0x1F636;$i++)
	{
		$content .= '<button class="btn btn-dark bot" onclick="emot('."'&#x".dechex($i)."'".');">&#x'.dechex($i).'</button>';
	}
	for ($i=0x2702;$i<=0x27B0;$i++)
	{
		$content .= '<button class="btn btn-dark bot" onclick="emot('."'&#x".dechex($i)."'".');">&#x'.dechex($i).'</button>';
	}
	for ($i=0x1F30D;$i<=0x1F567;$i++)
	{
		$content .= '<button class="btn btn-dark bot" onclick="emot('."'&#x".dechex($i)."'".');">&#x'.dechex($i).'</button>';
	}
	$id = bin2hex(random_bytes(16));
	$content .= '
</div>
<div class="pt-2">
<form method="post" action="/rcpt/'.$id.'" id="mmo">
<textarea name="msg" id="nda" class="form-control input-sm text-light bg-dark" style="min-height:200px;"></textarea>
<div class="text-right pb-3">
<button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
<form method="post" action="/rcpt/'.$id.'" class="dropzone" id="fmo">
<div class="btn btn-success dz-message" data-dz-message><span>Upload Attachments</span></div>
</form>
</div>
<div class="pt-3"></div>
';
}


$sql = "SELECT * FROM outbox WHERE is_deleted!='Y' ORDER BY idx DESC";
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$msg = json_decode($row['object'],true);
	switch ($row['item_type'])
	{
		case ACTIVITY_SHARE:
			$sql = "SELECT content FROM object_cache WHERE request_idx='".
				mysqli_real_escape_string($conn,$row['idx'])."'";
			$xres = mysqli_query($conn,$sql);
			$xrow = mysqli_fetch_array($xres);
			$xc = json_decode($xrow['content'],true);
			mysqli_free_result($xres);
			$details=array();
			$details['icon']['url']='/SOLOPOR/nobody.png';
			$details['url']='#';
			$details['name']='nobody';

			$sql = "SELECT content FROM profiles WHERE content LIKE '%".
				mysqli_real_escape_string($conn,$xc['attributedTo'])."%'";
			$jres = mysqli_query($conn,$sql);
			if (mysqli_num_rows($jres)>0)
			{
				$jrow = mysqli_fetch_array($jres,MYSQLI_ASSOC);
				$details = json_decode($jrow['content'],true);
			}
			mysqli_free_result($jres);
			$attach = $xc['attachment'];
			$attachments = array();

			if (is_array($attach))
			{
				foreach ($attach as $k=>$v)
				{
					switch ($v['mediaType'])
					{
						case 'image/jpeg': /* fall-through */
						case 'image/png': /* fall-through */
						case 'image/gif':
							$attachments[]='
<div style="padding:5px;"><img src="'.$v['url'].'" style="max-width:100%;height:auto;"></div>
';
							break;
						default:
        					$attachments[]='
<div style="padding:5px;"><a href="'.$v['url'].'" class="text-danger">'.htmlentities($v['url']).'</a></div>
';
							break;
					}
				}
			}

			if ($authenticated)
			{
				$share = '<a href="/share/'.$row['inbox_idx'].'" class="tt">re-share</a>';
			}
			$content .= '
<a name="R'.$row['idx'].'"></a>
<div class="outbox-msg">
<div class="outbox-headline-announcement">
<div>
<b><i class="fas fa-retweet"></i> Shared Post</b>
</div>
<div>
<div style="width:70px;float:right;padding:5px;text-align:right;" class="small">'.$share.'</div>
<b>url:</b> <a class="tt" href="'.$msg['object'].'">'.htmlentities($msg['object']).'</a><br>
<b>pub date:</b> '.$msg['published'].'<br>
<b>source date:</b> '.htmlentities($xc['published']).'<br>
</div>
</div>
<div class="outbox-body">
<div>
<a href="'.$details['url'].'"><img src="'.$details['icon']['url'].'" style="width:100px;height:auto;border-radius:5px;"></a>
<a href="'.$details['url'].'" class="text-warning"><b>'.htmlentities($details['name']).'</b></a><br>
</div>
'.$xc['content'].'
'.join("\n",$attachments).'
<div class="text-right small"><a href="javascript:void(0);" onclick="show('.$row['idx'].');">source</a></div>
<div id="m'.$row['idx'].'" style="display:none;">
<pre style="color:#fff;">'.print_r($msg,true).'</pre>
</div>
</div>
</div>
';
		break;
	}
}
mysqli_free_result($res);


