<?php

barf();

if ($authenticated)
{
	require('../LDSignatures.php');

	$inbox_idx = intval($page[2]);
	if ($inbox_idx>0)
	{
		$sql = "SELECT * FROM inbox WHERE idx='".mysqli_real_escape_string($conn,$inbox_idx)."'";
		$res = mysqli_query($conn,$sql);
		if (mysqli_num_rows($res)>0)
		{
			$row = mysqli_fetch_array($res);
						
			$in_msg = json_decode($row['content'],true);
			$object_url = $in_msg['object']['url'];
			$notify = $in_msg['actor'];

			$msgid = 'https://'.DOMAIN.'/status/'.bin2hex(random_bytes(16));

			$keys = json_decode(file_get_contents(KEYPATH),true);
			$p = json_decode(file_get_contents(ACTORPATH),true);
			$actor = $p['url'];

			$channel=array();
			$channel['prvkey']=$keys['prvkey'];
			$channel['url']=$actor;

			$a=array();
			$a['@context'] = 'https://www.w3.org/ns/activitystreams';
			$a['id'] = $msgid;
			$a['type'] = 'Announce';
			$a['actor'] = $actor;
			$a['published'] = gmdate('Y-m-d\TH:i:s\Z', time());
			$a['to'] = [ 'https://www.w3.org/ns/activitystreams#Public' ];
			$a['cc'] = [ $notify, 'https://'.DOMAIN.'/followers/'.USER ];
			$a['object'] = $object_url;
			$a['atomUri'] = $msgid;
			$a['signature'] = LDSignatures::sign($a,$channel);

			$sql = "INSERT INTO outbox (idx,inbox_idx,item_id,item_type,is_deleted,published,s_to,s_cc,object,processed) VALUES (NULL,'".
				mysqli_real_escape_string($conn,$inbox_idx)."','".
				mysqli_real_escape_string($conn,$msgid)."','".
				mysqli_real_escape_string($conn,$a['type'])."','N','".
				mysqli_real_escape_string($conn,time())."','".
				mysqli_real_escape_string($conn,json_encode($a['to']))."','".
				mysqli_real_escape_string($conn,json_encode($a['cc']))."','".
				mysqli_real_escape_string($conn,json_encode($a))."','0')";
				mysqli_query($conn,$sql);
			$req_idx = mysqli_insert_id($conn);

			$sql = "INSERT INTO object_cache (idx,request_idx,item_id,content,created,updated) VALUES (NULL,'".
				mysqli_real_escape_string($conn,$req_idx)."','".
				mysqli_real_escape_string($conn,$object_url)."','','".time()."','0')";
				mysqli_query($conn,$sql);
							
			$loc = $_SERVER['HTTP_REFERER'];
			$content = '<h1>Success</h1>
<p>Your post was submitted to outbox.</p>
<p><a href="'.$loc.'#R'.$inbox_idx.'" class="btn btn-primary">Return</a></p>
';
		}
		mysqli_free_result($res);
	}
}

