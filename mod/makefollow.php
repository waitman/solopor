<?php

define('SIGNIT',false);

barf();

if (!$authenticated)
{
	mysqli_close($conn);
	Header("Location: /login");
	exit();
} else {
	$target = rs('target',128);
	$sql = "SELECT idx FROM profiles WHERE url='".
		mysqli_real_escape_string($conn,$target)."' OR url='".
		mysqli_real_escape_string($conn,str_replace('/@','/channel/',$target))."'";
	$res = mysqli_query($conn,$sql);
	if (mysqli_num_rows($res)>0)
	{
		$content = '<h3>OOPs</h3><p>The profile at url '.htmlentities($target).' is already in the database.</p>';
	} else {

		$shop = 'N';
		$photo = 'N';
		$private ='N';
		if ($_POST['photo']=='Y') $photo='Y';
		if ($_POST['shop']=='Y') $shop='Y';
		if ($_POST['private']=='Y') $private='Y';

		$ch = curl_init($target);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.LDJSON_HEADER));
		$result = curl_exec($ch);

		$sql = "INSERT INTO profiles (idx,url,created,approved,pending,following,follower,updated,deleted,private,shop,photo,content,request) VALUES (NULL,'".
			mysqli_real_escape_string($conn,$target)."','".
			mysqli_real_escape_string($conn,time())."','0','0','N','N','0','0','".
			mysqli_real_escape_string($conn,$private)."','".
			mysqli_real_escape_string($conn,$shop)."','".
			mysqli_real_escape_string($conn,$photo)."','".
			mysqli_real_escape_string($conn,$result)."','')";
		mysqli_query($conn,$sql) or die($sql);

		$profile_idx = mysqli_insert_id($conn);
		if ($profile_idx<1) exit('error insert profile');


		require_once('LDSignatures.php');
		$keys = json_decode(file_get_contents(KEYPATH),true);
		$p = json_decode(file_get_contents(ACTORPATH),true);
		$actor = $p['url'];
		$channel=array();
		$channel['prvkey']=$keys['prvkey'];
		$channel['url']=$actor;
		$jd = json_decode($result,true);
		$inbox = $jd['inbox'];
		$msg_id = 'https://solopor.com/msgid/'.bin2hex(random_bytes(16));


		if (SIGNIT) {
			$msg = array_merge(['@context' => [
				ACTIVITYSTREAMS_JSONLD_REV,
				'https://w3id.org/security/v1'
				]],
				[
					'id'     => $msg_id,
					'type'   => ACTIVITY_FOLLOW,
					'actor'  => $actor,
					'object' => $target,
					'to'     => [ $addr ]
				]);
				$msg['signature'] = LDSignatures::sign($msg,$channel);
		} else {
			$msg =   [
				'@context' => ACTIVITYSTREAMS_JSONLD_REV,
				'id'     => $msg_id,
				'type'   => ACTIVITY_FOLLOW,
        			'actor'  => $actor,
        			'object' => $target
			];
		}

		$jmsg = json_encode($msg, JSON_UNESCAPED_SLASHES);

		$sql = "INSERT INTO queue (idx,profile_idx,msg_id,posturl,created,delivered,scheduled,response,msg) VALUES (NULL,'".
        		mysqli_real_escape_string($conn,$profile_idx)."','".
        		mysqli_real_escape_string($conn,$msg_id)."','".
        		mysqli_real_escape_string($conn,$inbox)."','".time()."','0','".time()."','','".
        		mysqli_real_escape_string($conn,$jmsg)."')";
		mysqli_query($conn,$sql) or die($sql);

		$content = '<h3>Follow Request Queued</h3>
<p>Msg Id: '.htmlentities($msg_id).'</p>
';


	}
	mysqli_free_result($res);
}



