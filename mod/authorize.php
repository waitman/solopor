<?php

barf();

if ($authenticated)
{
	$idx  = intval($page[2]);
	if ($idx>0)
	{
		$sql = "SELECT * FROM profiles WHERE idx='".
			mysqli_real_escape_string($conn,$idx)."'";
		$res = mysqli_query($conn,$sql);
		if (mysqli_num_rows($res)>0)
		{
			$row = mysqli_fetch_array($res);
			$request = json_decode($row['request'],true);
			$profile = json_decode($row['content'],true);
			$inbox = $profile['inbox'];
			$keys = json_decode(file_get_contents(KEYPATH),true);                          
			$p = json_decode(file_get_contents(ACTORPATH),true);
			$actor = $p['url'];
	
			$a_msg_id = '';
	
			/* if we did not initiate the connection we will to ask them to follow us */
	
			$a_msg_id = 'https://'.DOMAIN.'/msgid/'.bin2hex(random_bytes(16));
			$msg =   [
				'@context' => ACTIVITYSTREAMS_JSONLD_REV,
				'id'     => $a_msg_id,
				'type'   => ACTIVITY_FOLLOW,
				'actor'  => $actor,
				'object' => $row['url']
			];
			$jmsg = json_encode($msg, JSON_UNESCAPED_SLASHES);

			$sql = "INSERT INTO queue (idx,profile_idx,msg_id,posturl,created,delivered,scheduled,response,msg) VALUES (NULL,'".
				mysqli_real_escape_string($conn,$idx)."','".
				mysqli_real_escape_string($conn,$a_msg_id)."','".
				mysqli_real_escape_string($conn,$inbox)."','".time()."','0','".time()."','','".
				mysqli_real_escape_string($conn,$jmsg)."')";
				mysqli_query($conn,$sql) or die($sql);

			/* let them know we accepted their request */

			$msg_id = 'https://'.DOMAIN.'/msgid/'.bin2hex(random_bytes(16));

			$msg = [
				'@context' => ACTIVITYSTREAMS_JSONLD_REV,
				'id'     => $msg_id,
				'type'   => ACTIVITY_ACCEPT,
				'actor'  => $actor,
				'object' => $request
			];
			$jmsg = json_encode($msg, JSON_UNESCAPED_SLASHES);

			$sql = "INSERT INTO queue (idx,profile_idx,msg_id,posturl,created,delivered,scheduled,response,msg) VALUES (NULL,'".
				mysqli_real_escape_string($conn,$idx)."','".
				mysqli_real_escape_string($conn,$msg_id)."','".
				mysqli_real_escape_string($conn,$inbox)."','".time()."','0','".time()."','','".
				mysqli_real_escape_string($conn,$jmsg)."')";
			mysqli_query($conn,$sql) or die($sql);

			$sql = "UPDATE profiles SET follower='Y',pending=0 WHERE idx='".
				mysqli_real_escape_string($conn,$idx)."'";
			mysqli_query($conn,$sql);
			$content = '<h1>Message Queued</h1><p>'.$a_msg_id.'<br>'.$msg_id.'</p>';
		}
		mysqli_free_result($res);
	}
}

