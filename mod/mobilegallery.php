<?php

barf();

if ($authenticated)
{
	$page = intval($page[2]);
	$start = $page*30;
	$sql = "SELECT COUNT(idx) FROM gallery WHERE is_delete!='Y'";
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	$num_pages = ceil($row[0]/30);
	$num_images = $row[0];
	mysqli_free_result($res);

	$ops = array();
	for ($i=0;$i<$num_pages;$i++)
	{
		$sel = '';
		if ($page==$i) $sel = ' selected';
		$ops[]='<option value="'.$i.'"'.$sel.'>'.($i+1).'</option>';
	}
	$content = '<h1>Gallery '.$num_images.'</h1>
<script>
function changesel(v)
{
	var selv = $("#dulco"+v).children("option:selected").val();
	window.location= "/mobilegallery/" + selv;
}
</script>
<div class="p-3">
<select id="dulco1" onchange="changesel(1);" class="form-control">'.join('',$ops).'</select>
</div>
<div class="row">
';
	$sql = "SELECT * FROM gallery ORDER BY idx DESC LIMIT ".$start.",30";
	$res = mysqli_query($conn,$sql);
	while ($row = mysqli_fetch_array($res))
	{
		$content .= '
<div class="text-center">
<img src="/photos/'.$row['dt'].'/'.$row['image'].'.jpg" class="img-fluid" alt="'.$row['idx'].':'.$row['inbox_idx'].'">
</div>
';
	}
	mysqli_free_result($res);
	$content .= '
</div>
<div class="p-3">
<select id="dulco2" onchange="changesel(2);" class="form-control">'.join('',$ops).'</select>
</div>
';

}

