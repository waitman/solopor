<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$idx = intval($argv[1]);
if ($idx>0)
{
	$sql = "SELECT * FROM profiles WHERE idx='".
		mysqli_real_escape_string($conn,$idx)."'";
} else {
	$sql = "SELECT * FROM profiles WHERE deleted<1 ORDER BY idx DESC";
}
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$addr = $row['url'];

	$ch = curl_init($addr);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.LDJSON_HEADER));
	$result = curl_exec($ch);


	$sql = "UPDATE profiles SET content='".
		mysqli_real_escape_string($conn,$result)."',".
		"updated='".time()."' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."' AND content!='".mysqli_real_escape_string($conn,$result)."'";

	mysqli_query($conn,$sql) or die($sql);
}

mysqli_free_result($res);
mysqli_close($conn);

