<?php

define('DBFILE','/www/work/db.php');				// path to db.php, probably just db.php . i put this here for GIT.
define('KEYPATH','/usr/local/etc/wago.openssl');		// path to store your keys, NOT PUBLIC - has private key. should not be in web path
define('ACTORPATH','/usr/local/etc/wago.actor');		// path to store your actor file. public OK = its public anyway?
define('WEBFINGERPATH','/usr/local/etc/wago.webfinger');	// path to store your webfinger file, public OK = its public anyway?
define('WGET','/usr/local/bin/wget');
define('CONVERT','/usr/local/bin/convert');
define('WWWPATH','/www/solopor');
define('USER','wago');
define('DOMAIN','solopor.com');


define ('LDJSON_HEADER','application/ld+json; profile="https://www.w3.org/ns/activitystreams"');
define ('ACTIVITY_HEADER','application/activity+json, application/ld+json; profile="https://www.w3.org/ns/activitystreams"');

// this stuff is copied from ZOT. not sure which constants will be used in this project.



define ('NAMESPACE_ACTIVITY_SCHEMA', 'http://activitystrea.ms/schema/1.0/');

define ('ACTIVITYSTREAMS_JSONLD_REV','https://www.w3.org/ns/activitystreams');

define ('ACTIVITY_PUBLIC_INBOX','https://www.w3.org/ns/activitystreams#Public');

define ('ACTIVITY_POST',        'Create' );
define ('ACTIVITY_CREATE',      'Create' );
define ('ACTIVITY_UPDATE',      'Update' );
define ('ACTIVITY_LIKE',        'Like' );
define ('ACTIVITY_DISLIKE',     'Dislike' );
define ('ACTIVITY_SHARE',       'Announce' );
define ('ACTIVITY_FOLLOW',      'Follow' );
define ('ACTIVITY_UNFOLLOW',    'Unfollow');
define ('ACTIVITY_ACCEPT',	'Accept');
define ('ACTIVITY_DELETE',	'Delete');
define ('ACTIVITY_UNDO',	'Undo');
define ('ACTIVITY_REJECT',	'Reject');


define ('ACTIVITY_OBJ_COMMENT', 'Note' );
define ('ACTIVITY_OBJ_NOTE',    'Note' );
define ('ACTIVITY_OBJ_ARTICLE', 'Article' );
define ('ACTIVITY_OBJ_PERSON',  'Person' );
define ('ACTIVITY_OBJ_PHOTO',   'Image');
define ('ACTIVITY_OBJ_P_PHOTO', 'Icon' );
define ('ACTIVITY_OBJ_PROFILE', 'Profile');
define ('ACTIVITY_OBJ_EVENT',   'Event' );
define ('ACTIVITY_OBJ_POLL',    'Question');

define ('ACTIVITY_FRIEND',      NAMESPACE_ACTIVITY_SCHEMA . 'make-friend' );
define ('ACTIVITY_REQ_FRIEND',  NAMESPACE_ACTIVITY_SCHEMA . 'request-friend' );
define ('ACTIVITY_UNFRIEND',    NAMESPACE_ACTIVITY_SCHEMA . 'remove-friend' );
define ('ACTIVITY_JOIN',        NAMESPACE_ACTIVITY_SCHEMA . 'join' );

define ('ACTIVITY_FAVORITE',    NAMESPACE_ACTIVITY_SCHEMA . 'favorite' );
define ('ACTIVITY_WIN',         NAMESPACE_ACTIVITY_SCHEMA . 'win' );
define ('ACTIVITY_LOSE',        NAMESPACE_ACTIVITY_SCHEMA . 'lose' );
define ('ACTIVITY_TIE',         NAMESPACE_ACTIVITY_SCHEMA . 'tie' );
define ('ACTIVITY_COMPLETE',    NAMESPACE_ACTIVITY_SCHEMA . 'complete' );
define ('ACTIVITY_TAG',         NAMESPACE_ACTIVITY_SCHEMA . 'tag' );

define ('ACTIVITY_OBJ_ACTIVITY',NAMESPACE_ACTIVITY_SCHEMA . 'activity' );
define ('ACTIVITY_OBJ_ALBUM',   NAMESPACE_ACTIVITY_SCHEMA . 'photo-album' );
define ('ACTIVITY_OBJ_GROUP',   NAMESPACE_ACTIVITY_SCHEMA . 'group' );
define ('ACTIVITY_OBJ_GAME',    NAMESPACE_ACTIVITY_SCHEMA . 'game' );
define ('ACTIVITY_OBJ_WIKI',    NAMESPACE_ACTIVITY_SCHEMA . 'wiki' );


