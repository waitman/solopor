<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');

$openssl_options = [
	'digest_alg'       => 'sha1',
	'private_key_bits' => 4096,
	'encrypt_key'      => false
];

$result = openssl_pkey_new($openssl_options);

$response = [ 'prvkey' => '', 'pubkey' => '' ];
openssl_pkey_export($result, $response['prvkey']);
$pkey = openssl_pkey_get_details($result);
$response['pubkey'] = $pkey["key"];

file_put_contents(KEYPATH,json_encode($response));

echo "\n\n".'Keypair written to '.KEYPATH."\n\n";

