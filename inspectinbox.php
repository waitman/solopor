<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$idx = intval($argv[1]);

$sql = "SELECT * FROM inbox WHERE idx='".mysqli_real_escape_string($conn,$idx)."'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);


echo 'IP:	'.$row['ip']."\n\n";

echo 'User:	'.$row['user']."\n\n";

echo 'Created:	'.date('n/j/Y g:i a',$row['created'])."\n\n";

echo 'Seen:	'.date('n/j/Y g:i a',$row['seen'])."\n\n";

echo 'Processed:	'.date('n/j/Y g:i a',$row['processed'])."\n\n";

echo 'Errors:	'.$row['errors']."\n\n";


echo "Headers:\n\n";
$j = json_decode($row['headers'],true);
print_r($j);


echo "\n\nContent:\n\n";

$j = json_decode($row['content'],true);
print_r($j);

echo "\n\n";

mysqli_free_result($res);
mysqli_close($conn);




