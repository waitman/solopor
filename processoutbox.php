<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);


$add=array();
$profiles = array();
$sql = "SELECT * FROM outbox WHERE processed<1 AND is_deleted!='Y'";
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$sql = "UPDATE outbox SET processed='".time()."' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."'";
	mysqli_query($conn,$sql);

	$to = json_decode($row['s_to'],true);
	foreach ($to as $k=>$v)
	{
		if ($v!='https://www.w3.org/ns/activitystreams#Public')
		{
			$sql = "SELECT * FROM profiles WHERE content LIKE '%".mysqli_real_escape_string($conn,$v)."%'";
                        $xres = mysqli_query($conn,$sql);
                        $xrow = mysqli_fetch_array($xres);
                        $prof = json_decode($xrow['content'],true);
                        $inbox = $prof['inbox'];
                        $add[]=$inbox;
                        $profiles[$inbox]=$xrow['idx'];
			mysqli_free_result($xres);
		}
	}
	$cc = json_decode($row['s_cc'],true);
	foreach ($cc as $k=>$v)
	{
		if ($v=='https://solopor.com/followers/wago')
		{
			$sql = "SELECT * FROM profiles WHERE follower='Y' AND deleted<1";
			$xres = mysqli_query($conn,$sql);
			while ($xrow = mysqli_fetch_array($xres))
			{
				$prof = json_decode($xrow['content'],true);
				$inbox = $prof['inbox'];
				$add[]=$inbox;
				$profiles[$inbox]=$xrow['idx'];
			}
			mysqli_free_result($xres);
		} else {
			$sql = "SELECT * FROM profiles WHERE content LIKE '%".mysqli_real_escape_string($conn,$v)."%'";
			$xres = mysqli_query($conn,$sql);
			$xrow = mysqli_fetch_array($xres);
			$prof = json_decode($xrow['content'],true);
			$inbox = $prof['inbox'];
			$add[]=$inbox;
			$profiles[$inbox]=$xrow['idx'];
			mysqli_free_result($xres);
		}
	}
	foreach ($add as $k=>$v)
	{
		$msg_id = 'https://solopor.com/msgid/'.bin2hex(random_bytes(16));
		$sql = "INSERT INTO queue (idx,profile_idx,msg_id,posturl,created,delivered,scheduled,response,msg) VALUES (NULL,'".
				mysqli_real_escape_string($conn,intval($profiles[$v]))."','".
				mysqli_real_escape_string($conn,$msg_id)."','".
				mysqli_real_escape_string($conn,$v)."','".time()."',0,'".time()."','','".
				mysqli_real_escape_string($conn,$row['object'])."')";
		mysqli_query($conn,$sql) or die($sql);
	}
}
mysqli_free_result($res);
mysqli_close($conn);


