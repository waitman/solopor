<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$check = strtotime('-6 hours');

$sql = "SELECT * FROM object_cache WHERE updated<".mysqli_real_escape_string($conn,$check);
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$ch = curl_init($row['item_id']);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.ACTIVITY_HEADER));
	$result = curl_exec($ch);
	$sql = "UPDATE object_cache SET content='".
		mysqli_real_escape_string($conn,$result)."',".
		"updated='".time()."' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."'";
	mysqli_query($conn,$sql);
}
mysqli_free_result($res);
mysqli_close($conn);

