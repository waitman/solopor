<?php
include('config.php');
include(DBFILE);

require('LDSignatures.php');
$msgid = 'https://solopor.com/status/'.bin2hex(random_bytes(16));
$keys = json_decode(file_get_contents(KEYPATH),true);
$p = json_decode(file_get_contents(ACTORPATH),true);
$actor = $p['url'];

$channel=array();
$channel['prvkey']=$keys['prvkey'];
$channel['url']=$actor;

$sql = "SELECT * FROM msgs WHERE processed='N'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);

$comp_id = $row['comp_id'];
$msg_raw = $row['msg'];

$sql = "UPDATE msgs SET processed='Y' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."'";
mysqli_query($conn,$sql);

$x = explode("\n",$msg_raw);
foreach ($x as $k=>$v)
{
	$x[$k]='<p>'.$v.'</p>';
}
$msg = join('',$x);

$a=array();
$a['@context'] = [ 'https://www.w3.org/ns/activitystreams', 'https://w3id.org/security/v1' ];
$a['id'] = $msgid.'/activity';
$a['type'] = 'Create';
$a['actor'] = $actor;
$a['published'] = gmdate('Y-m-d\TH:i:s\Z', time());
$a['to'] = [ 'https://www.w3.org/ns/activitystreams#Public' ];
$a['cc'] = [ 'https://solopor.com/followers/wago' ];
$a['object'] = $object_url;
$a['atomUri'] = $msgid;
$a['object'] = array();
$a['object']['id'] = $msgid;
$a['object']['type'] = 'Note';
$a['object']['summary'] = '';
$a['object']['inReplyTo'] = '';
$a['object']['published'] = gmdate('Y-m-d\TH:i:s\Z', time());
$a['object']['url'] = $msgid;
$a['object']['attributedTo'] = $actor;
$a['object']['to'] = [ 'https://www.w3.org/ns/activitystreams#Public' ];
$a['object']['cc'] = [ 'https://solopor.com/followers/wago' ];
$a['object']['sensitive'] = '';
$a['object']['atomUri'] = $msgid;
$a['object']['inReplyToAtomUri'] = '';
$a['object']['conversation']='tag:solorpor.com,'.gmdate('Y-m-d', time()).':objectId='.$comp_id.':objectType=Conversation';
$a['object']['attachment'] = array();

$sql = "SELECT * FROM attachments WHERE comp_id='".
		mysqli_real_escape_string($conn,$comp_id)."' AND is_deleted!='Y' ORDER BY idx ASC";
$xres = mysqli_query($conn,$sql);
while ($xrow = mysqli_fetch_array($xres))
{
	$attach = array();
	$attach['id']=$comp_id.'.'.$xrow['idx'];
	$attach['type']=$xrow['type'];
	$attach['url']='https://solopor.com/st/'.$xrow['dt'].'/'.$xrow['filename'];
	$attach['description']=$xrow['name'];
	if ($xrow['type']=='image')
	{
		if (!is_dir('st/'.$xrow['dt'].'/fu'))
		{
			mkdir('st/'.$xrow['dt'].'/fu');
		}
		system('convert -strip -size 300x300 -scale 300x300 -density 120 st/'.$xrow['dt'].'/'.$xrow['filename'].' st/'.$xrow['dt'].'/fu/'.$xrow['filename']);
		$attach['preview_url']='https://solopor.com/st/'.$xrow['dt'].'/fu/'.$xrow['filename'];
		$msg .= '<p><a href="https://solopor.com/st/'.$xrow['dt'].'/'.$xrow['filename'].'"><img src="https://solopor.com/st/'.$xrow['dt'].'/'.$xrow['filename'].'" style="max-width:100%;height:auto;" alt="Image/photo"></a></p>';
	} else {
		$attach['preview_url']='https://solopor.com/SOLOPOR/surprise.jpg';
		$msg .= '<p><a href="https://solopor.com/st/'.$xrow['dt'].'/'.$xrow['filename'].'"><img src="https://solopor.com/SOLOPOR/surprise.jpg" width="300" height="236" alt="mystery"></a></p>';
	}
	$a['object']['attachment'][]=$attach;
}
mysqli_free_result($xres);
$a['object']['tag'] = array();
$a['object']['replies'] = array();
$a['object']['replies']['id'] = $msgid.'/replies';
$a['object']['replies']['type'] = 'Collection';
$a['object']['replies']['first'] = array();
$a['object']['replies']['first']['type'] = 'CollectionPage';
$a['object']['replies']['first']['next'] = $msgid.'/replies/1';
$a['object']['replies']['first']['partOf'] = $msgid.'/replies';
$a['object']['replies']['first']['items'] = array();


$a['object']['content']=$msg;
$a['object']['contentMap'] = [ 'en' => $msg ];


$a['signature'] = LDSignatures::sign($a,$channel);

$out =  json_encode($a);

$inbox_idx = 0;

$sql = "INSERT INTO outbox (idx,inbox_idx,item_id,item_type,is_deleted,published,s_to,s_cc,object,processed) VALUES (NULL,'".
		mysqli_real_escape_string($conn,$inbox_idx)."','".
		mysqli_real_escape_string($conn,$msgid)."','".
		mysqli_real_escape_string($conn,$a['type'])."','N','".
		mysqli_real_escape_string($conn,time())."','".
		mysqli_real_escape_string($conn,json_encode($a['to']))."','".
		mysqli_real_escape_string($conn,json_encode($a['cc']))."','".
		mysqli_real_escape_string($conn,json_encode($a))."','0')";
mysqli_query($conn,$sql);
$req_idx = mysqli_insert_id($conn);

mysqli_free_result($res);
mysqli_close($conn);


