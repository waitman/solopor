<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

require('HTTPSig.php');
include('config.php');
include(DBFILE);

$keys = json_decode(file_get_contents(KEYPATH),true);

$p = json_decode(file_get_contents(ACTORPATH),true);
$actor = $p['url'];

$msgid = $argv[1];
$auto = false;

if ($msgid=='auto')
{
	$msgid='';
	$auto = true;
}

if ($msgid=='')
{
	$msgid='';
	$sql = "SELECT msg_id FROM queue WHERE delivered=0 AND scheduled>0";
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	$msgid = $row['msg_id'];
	mysqli_free_result($res);
}

if ($msgid=='') 
{
	if ($auto) exit();
	exit('no msg id');
}

$sql = "SELECT * FROM queue WHERE msg_id='".
	mysqli_real_escape_string($conn,$msgid)."'";
$res = mysqli_query($conn,$sql);
if (mysqli_num_rows($res)<1)
{
	exit ("\nMessage does not exist.\n\n");
} else {
	$row = mysqli_fetch_array($res);
	
	$posturl = $row['posturl'];

	$tg=explode('/',$posturl);
	array_shift($tg);
	array_shift($tg);
	array_shift($tg);
	$target = '/'.join('/',$tg);

	$ret = $row['msg'];

	$date = new DateTime();
	$date->setTimezone(new DateTimeZone('UTC'));
	$headers = array();
	$headers['Content-Type'] = 'application/activity+json';
	$headers['Date'] = str_replace('+0000','GMT',$date->format(DateTimeInterface::RFC1123));
	$headers['Digest'] = HTTPSig::generate_digest_header($ret);
	$headers['(request-target)'] = 'post '.$target;
	$xhead = HTTPSig::create_sig($headers,$keys['prvkey'],$actor);


	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $posturl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $ret);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $xhead);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; solopor)");

	$result = curl_exec($ch);

	$sql = "UPDATE queue SET delivered='".time()."',response='".mysqli_real_escape_string($conn,$result)."' WHERE idx='".
		mysqli_real_escape_string($conn,$row['idx'])."'";
	mysqli_query($conn,$sql);

	echo $result;
}
mysqli_free_result($res);
mysqli_close($conn);


