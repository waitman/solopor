<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

include('config.php');
include(DBFILE);

$idx = intval($argv[1]);

if ($idx<1) 
{
	$sql = "SELECT * FROM profiles ORDER BY idx DESC";
} else {
	$sql = "SELECT * FROM profiles WHERE idx='".mysqli_real_escape_string($conn,$idx)."'";
}
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($res);


echo 'Created Date:	'.date('n/j/Y g:i a',$row['created'])."\n\n";
echo 'Approved Date:     '.date('n/j/Y g:i a',$row['approved'])."\n\n";
echo 'Pending Date:     '.date('n/j/Y g:i a',$row['pending'])."\n\n";
echo 'Updated Date:     '.date('n/j/Y g:i a',$row['updated'])."\n\n";
echo 'Deleted Date:	'.date('n/j/Y g:i a',$row['deleted'])."\n\n";
echo 'Following:	'.$row['following']."\n\n";
echo 'Follower:		'.$row['follower']."\n\n";
echo 'Private:		'.$row['private']."\n\n";
echo 'Shop:		'.$row['shop']."\n\n";
echo 'Photo:		'.$row['photo']."\n\n";


echo "Content:\n\n";
$j = json_decode($row['content'],true);
print_r($j);
echo "\n\n";

echo "Request:\n\n";
$j = json_decode($row['request'],true);
print_r($j);



echo "\n\n";


mysqli_free_result($res);
mysqli_close($conn);




