-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for FreeBSD12.1 (amd64)
--
-- Host: localhost    Database: solopor
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accnt`
--

DROP TABLE IF EXISTS `accnt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accnt` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(64) NOT NULL,
  `lname` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `newsletter` enum('N','Y') NOT NULL DEFAULT 'N',
  `sequence` int(11) NOT NULL,
  `reset_code` varchar(32) NOT NULL,
  `activate_code` varchar(32) NOT NULL,
  `loyalty_points` int(10) unsigned NOT NULL,
  `saved_bi` text NOT NULL,
  `saved_si` text NOT NULL,
  `currency` enum('USD','DOP') NOT NULL DEFAULT 'USD',
  `is_god` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_cache`
--

DROP TABLE IF EXISTS `announcement_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_cache` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_idx` int(10) unsigned NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=7134 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_id` varchar(64) NOT NULL,
  `dt` date NOT NULL,
  `type` varchar(32) NOT NULL,
  `mediaType` varchar(32) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_deleted` enum('N','Y') NOT NULL DEFAULT 'N',
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `inbox_idx` bigint(20) unsigned NOT NULL,
  `dt` date NOT NULL,
  `image` varchar(64) NOT NULL,
  `is_delete` enum('N','Y') NOT NULL DEFAULT 'N',
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=3469 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbox` (
  `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(64) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `seen` int(10) unsigned NOT NULL,
  `processed` int(10) unsigned NOT NULL,
  `errors` enum('N','Y') NOT NULL DEFAULT 'N',
  `headers` text NOT NULL,
  `content` text NOT NULL,
  `post_process` enum('N','Y') NOT NULL DEFAULT 'N',
  `cache` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=13558 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logins` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accountidx` int(10) unsigned NOT NULL,
  `is_paypal` enum('N','Y') NOT NULL DEFAULT 'N',
  `sessionid` varchar(64) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `logout` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `msgs`
--

DROP TABLE IF EXISTS `msgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msgs` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_id` varchar(64) NOT NULL,
  `msg` text NOT NULL,
  `processed` enum('N','Y') NOT NULL DEFAULT 'N',
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_cache`
--

DROP TABLE IF EXISTS `object_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_cache` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_idx` int(10) unsigned NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `outbox`
--

DROP TABLE IF EXISTS `outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbox` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inbox_idx` int(10) unsigned NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `item_type` varchar(32) NOT NULL,
  `is_deleted` enum('N','Y') NOT NULL DEFAULT 'N',
  `published` int(10) unsigned NOT NULL,
  `s_to` varchar(255) NOT NULL,
  `s_cc` varchar(255) NOT NULL,
  `object` text NOT NULL,
  `processed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `approved` int(10) unsigned NOT NULL,
  `pending` int(10) unsigned NOT NULL,
  `following` enum('N','Y') NOT NULL DEFAULT 'N',
  `follower` enum('N','Y') NOT NULL DEFAULT 'N',
  `updated` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL,
  `private` enum('N','Y') NOT NULL DEFAULT 'N',
  `shop` enum('N','Y') NOT NULL DEFAULT 'N',
  `photo` enum('N','Y') NOT NULL DEFAULT 'N',
  `content` text NOT NULL,
  `request` text NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_idx` int(10) unsigned NOT NULL,
  `msg_id` varchar(255) NOT NULL,
  `posturl` varchar(255) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `delivered` int(10) unsigned NOT NULL,
  `scheduled` int(10) unsigned NOT NULL,
  `response` text NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reqs`
--

DROP TABLE IF EXISTS `reqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reqs` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) NOT NULL,
  `req` varchar(255) NOT NULL,
  `srv` text NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=25327 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stmpd`
--

DROP TABLE IF EXISTS `stmpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stmpd` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) NOT NULL,
  `req` varchar(255) NOT NULL,
  `hdr` text NOT NULL,
  `cnt` text NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `processed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmpd`
--

DROP TABLE IF EXISTS `tmpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpd` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) NOT NULL,
  `req` varchar(255) NOT NULL,
  `hdr` text NOT NULL,
  `cnt` text NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=15520 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-03 16:44:23
