<?php

function barf() {}

function rs($key,$len)
{
        return (substr(trim(filter_var($_POST[$key],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH)),0,$len));
}

function ers($val,$len)
{
        return (substr(trim(filter_var($val,FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH)),0,$len));
}



$authenticated=false;
$private = false;

$s = $_SERVER['REQUEST_URI'];
if (strstr($s,'?'))
{
	$x=explode('?',$s);
	$s=array_shift($x);
}

$page=explode('/',$s);

if ($page[1]=='') $page[1]='outbox';

include('config.php');
include(DBFILE);

/* Save All Reqs for Debug */
$sql = "INSERT INTO reqs (idx,ip,req,srv,sequence) VALUES (NULL,'".
		mysqli_real_escape_string($conn,$_SERVER['REMOTE_ADDR'])."','".
		mysqli_real_escape_string($conn,$_SERVER['REQUEST_URI'])."','".
		mysqli_real_escape_string($conn,json_encode($_SERVER))."','".
		time()."')";
mysqli_query($conn,$sql);

if ($s=='/api/v1/instance')
{
	include('mod/api_v1_instance.php');
	exit();
}

if (($s=='/nodeinfo/2.0.json')||($s=='/nodeinfo/2.0'))
{
	include('mod/nodeinfo2.php');
	exit();
}

if ($s=='/.well-known/nodeinfo')
{
	include('mod/nodeinfo.php');
	exit();
}

if (strstr($_SERVER['HTTP_ACCEPT'],'json'))
{

	/* ActivityPub */
	
	switch ($page[1])
	{
		case 'rcpt':
			include('mod/rcpt.php');
			exit();
			break;

		case USER:
			include('mod/ap_wago.php');
			exit();
			break;

		case 'folowing': /* fall-through */
		case 'following':
			include('mod/ap_following.php');
			exit();
			break;

		case 'followers':
			include('mod/ap_followers.php');
			exit();
			break;

		case 'outbox':
			include('mod/ap_outbox.php');
			exit();
			break;

		case 'inbox':
			include('mod/ap_inbox.php');
			exit();
			break;
	}

} else {

	/* HTML */
	
	$content = '';
	include('login.php');
	switch ($page[1])
    {
		case '.well-known':
			include('mod/well-known.php');
			exit();
			break;

		case 'makefollow':
			include('mod/makefollow.php');
			break;

		case 'follow':
			include('mod/follow.php');
			break;

		case 'authorize':
			include('mod/authorize.php');
			break;
						
		case 'logout':
			include('mod/logout.php');
			/* fall-through for login page */

		case 'login':
			include('mod/login.php');
			break;

		case 'contact':
			include('mod/contact.php');
			break;

		case '@'.USER: /*fall-through*/
		case 'profile':
			include('mod/profile.php');
			break;

		case 'gallery':
			include('mod/gallery.php');
			break;

		case 'mobilegallery':
			include('mod/mobilegallery.php');
			break;

		case 'connections':
			include('mod/connections.php');
			break;

		case 'dofollow':
			include('mod/dofollow.php');
			break;

		case 'chkfollow':
			include('mod/chkfollow.php');
			break;

		case 'share':
			include('mod/share.php');
			break;

		case 'status':
			include('mod/status.php');
			break;

		case 'rcpt':
			include('mod/html_rcpt.php');
			break;

		case USER: /* fall-through */
		case 'outbox': //OUTBOX
			include('mod/outbox.php');
			break;

		case 'private':
			$private = true;
			/* fall-through */

		case 'inbox': //INBOX
			include('mod/inbox.php');
			break;

		default:
			break;
	}

	$layout = file_get_contents('layout.html');
	$layout = str_replace('<!--Content-->',$content,$layout);
	if ($authenticated)
	{
		$layout = str_replace('<a class="nav-link btn btn-dark" href="/login"><i class="fas fa-sign-in-alt"></i> Login</a>',
			'<a class="nav-link btn btn-dark" href="/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>',
			$layout);
	}
	mysqli_close($conn);
	echo $layout;
}


