<?php

if (php_sapi_name() != "cli")
{
        exit('cli access only.');
}

require('HTTPSig.php');
include('config.php');
include(DBFILE);

$sql = "SELECT * FROM inbox WHERE processed=0 AND seen=0";
$res = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($res))
{
	$content = $row['content'];

	$xh = json_decode($row['headers'],true);
	$headers = array();
	$headers['(request-target)'] = strtolower($xh['REQUEST_METHOD']).' '.$xh['REQUEST_URI'];
	$headers['content-type'] = $xh['CONTENT_TYPE'];
	$headers['content-length'] = $xh['CONTENT_LENGTH'];
	foreach ($xh as $k=>$v)
	{
		if (strpos($k,'HTTP_') === 0)
		{
			$field = str_replace('_','-',strtolower(substr($k,5)));
			$headers[$field] = $v;
		}
	}
	
	$hs = explode(',',$headers['signature']);
	$sig = array();
	if (count($hs)<1)
	{
		//no signature
	} else {
		foreach ($hs as $v)
		{
			$rr=explode('=',$v);
			$sig[trim(strtolower($rr[0]))]=trim(str_replace('"','',$rr[1]));
		}
	}


	if (array_key_exists('keyid',$sig) && ($sig['keyid']!='') && (substr($sig['keyid'],0,8)=='https://'))
	{
		$tg=explode('/',$sig['keyid']);
		array_shift($tg);
		array_shift($tg);
		$host = array_shift($tg);
		$target = '/'.join('/',$tg);

		$my_keys = json_decode(file_get_contents(KEYPATH),true);
		$p = json_decode(file_get_contents(ACTORPATH),true);
		$my_actor = $p['url'];

		$req_headers = [
			'Accept'		=> ACTIVITY_HEADER,
			'Host'			=> $host,
			'Date'			=> gmdate('Y-m-d\TH:i:s\Z', time()),
			'(request-target)'	=> 'get ' . $target
		];
    
		$req_h = HTTPSig::create_sig($req_headers,$my_keys['prvkey'],$my_actor);

		$ch = curl_init($sig['keyid']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $req_h);
		$result = curl_exec($ch);

		$xr = json_decode($result,true);
		$key = $xr['publicKey'];

		$result = HTTPSig::verify($headers,$key,$content);

		if ($result['header_signed'] && $result['header_valid'] && $result['content_signed'] && $result['content_valid'])
		{
			// ok
			$activity = json_decode($content,true);

			switch ($activity['type'])
			{

				case ACTIVITY_FOLLOW:
					
					$object = [
						'type' 		=> 'Follow',
						'id' 		=> $activity['id'],
						'actor' 	=> $activity['actor'],
						'object' 	=> $activity['object']
					];
					//check if we already have this person
					$sql = "SELECT idx,pending,follower FROM profiles WHERE url='".
						mysqli_real_escape_string($conn,$activity['actor'])."'";
					$xres = mysqli_query($conn,$sql);
					if (mysqli_num_rows($xres)>0)
					{
						$xrow = mysqli_fetch_array($xres);
						if ($xrow['pending']<1)
						{
							if ($xrow['folower']!='Y')
							{
								$sql = "UPDATE profiles SET pending='".time()."',request='".
									mysqli_real_escape_string($conn,json_encode($object))."' WHERE idx='".
									mysqli_real_escape_string($conn,$xrow['idx'])."'";
								mysqli_query($conn,$sql);
							}
						}
					} else {
						$sql = "INSERT INTO profiles (idx,url,created,approved,pending,following,follower,updated,deleted,content,request) VALUES (NULL,'".
							mysqli_real_escape_string($conn,$activity['actor'])."','".
							mysqli_real_escape_string($conn,time())."','0','".
							mysqli_real_escape_string($conn,time())."','N','N','0','0','','".
							mysqli_real_escape_string($conn,json_encode($object))."')";
						mysqli_query($conn,$sql);
						$this_idx = mysqli_insert_id($conn);
						if ($this_idx>0)
						{
							system('/usr/local/bin/php -q syncprofiles.php '.$this_idx);
						}
					}
					mysqli_free_result($xres);
					$sql = "UPDATE inbox SET processed='".time()."' WHERE idx='".
						mysqli_real_escape_string($conn,$row['idx'])."'";
					mysqli_query($conn,$sql);
					break;

				case ACTIVITY_ACCEPT:
					$actor = $activity['actor'];
					$object = $activity['object'];
					switch ($object['type'])
					{
						case ACTIVITY_FOLLOW:
							$id = $object['id'];
							if ($my_actor == $object['actor'])
							{
								$sql = "SELECT idx,profile_idx FROM queue WHERE msg_id='".
									mysqli_real_escape_string($conn,$id)."'";
								$xres = mysqli_query($conn,$sql);
								if (mysqli_num_rows($xres)>0)
								{
									$xrow = mysqli_fetch_array($xres);
									if ($xrow['profile_idx']>0)
									{
										$sql = "UPDATE profiles SET approved='".time()."',following='Y' WHERE idx='".
											mysqli_real_escape_string($conn,$xrow['profile_idx'])."'";
									} else {
										$sql = "UPDATE profiles SET approved='".time()."',following='Y' WHERE url='".
											mysqli_real_escape_string($conn,$object['object'])."'";
									}
									mysqli_query($conn,$sql);
									$sql = "UPDATE inbox SET processed='".time()."' WHERE idx='".
										mysqli_real_escape_string($conn,$row['idx'])."'";
									mysqli_query($conn,$sql);
								}
								mysqli_free_result($xres);
							}
							break;
					}
					break;

				default:
					echo "\n".$row['idx']."-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--"."\n";
/*
					print_r($activity);
					print_r($object);
*/
					break;
			}
			$sql = "UPDATE inbox SET seen='".time()."' WHERE idx='".
				mysqli_real_escape_string($conn,$row['idx'])."'";
			mysqli_query($conn,$sql);
		} else {
			//errors to check out
			$sql = "UPDATE inbox SET errors='Y',processed=".time().",seen='".time()."' WHERE idx='".
				mysqli_real_escape_string($conn,$row['idx'])."'";
			mysqli_query($conn,$sql);
		}

/*
[signer] => https://zap.dog/channel/wago
[portable_id] => 
[header_signed] => 1
[header_valid] => 1
[content_signed] => 1
[content_valid] => 1
*/

	}
}
mysqli_free_result($res);
mysqli_close($conn);
